<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>LibraryGridView</name>
    <message>
        <location filename="../LibraryGridView.qml" line="24"/>
        <source>Search %L1 Games...</source>
        <translation>Sök %L1 spel...</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="82"/>
        <source>All (%L1)</source>
        <translation>Alla (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="83"/>
        <source>Installed (%L1)</source>
        <translation>Installerad (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="84"/>
        <source>Recent (%L1)</source>
        <translation>Senaste (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="85"/>
        <source>Has Updates (%L1)</source>
        <translation>Har uppdateringar (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="86"/>
        <source>Processing (%L1)</source>
        <translation>Bearbetar (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="167"/>
        <source>Play</source>
        <translation>Spela</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="176"/>
        <source>Install</source>
        <translation type="unfinished">Installera</translation>
    </message>
    <message>
        <source>View In Store</source>
        <translation type="vanished">Visa i butik</translation>
    </message>
    <message>
        <source>Library</source>
        <translation type="vanished">Bibliotek</translation>
    </message>
    <message>
        <source>⋮</source>
        <translation type="vanished">⋮</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Inställningar</translation>
    </message>
    <message>
        <source>Check For Updates</source>
        <translation type="vanished">Sök efter uppdateringar</translation>
    </message>
    <message>
        <source>Update All</source>
        <translation type="vanished">Uppdatera alla</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Avsluta</translation>
    </message>
    <message>
        <source>You have operations pending.</source>
        <translation type="vanished">Du har väntande åtgärder.</translation>
    </message>
</context>
<context>
    <name>LibraryListView</name>
    <message>
        <source>Close Anyway</source>
        <translation type="vanished">Stäng ändå</translation>
    </message>
    <message>
        <source>All Games (%L1)</source>
        <translation type="vanished">Alla spel (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="109"/>
        <source>Installed (%L1)</source>
        <translation>Installerade (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="110"/>
        <source>Recent (%L1)</source>
        <translation>Senaste (%L1)</translation>
    </message>
    <message>
        <source>New (%L1)</source>
        <translation type="vanished">Nytt (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="111"/>
        <source>Has Updates (%L1)</source>
        <translation>Har uppdateringar (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="112"/>
        <source>Processing (%L1)</source>
        <translation>Bearbetar (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="299"/>
        <source>New</source>
        <translation>Nya</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="319"/>
        <source>Nothing seems to be here.</source>
        <translation>Ingenting verkar finnas här.</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="424"/>
        <source>Install</source>
        <translation>Installera</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="439"/>
        <source>Download Size:	%1</source>
        <translation>Nedladdningsstorlek:	%1</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="446"/>
        <source>Installed Size:	%1</source>
        <translation>Installerad storlek:	%1</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="453"/>
        <source>Install Game?</source>
        <translation>Installera spel?</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="484"/>
        <source>Play</source>
        <translation>Spela</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="484"/>
        <source>In-Game</source>
        <translation>I spelet</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="504"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="507"/>
        <source>Uninstall</source>
        <translation>Avinstallera</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="528"/>
        <source>Are you sure?</source>
        <translation>Är du säker?</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="778"/>
        <source>Description</source>
        <translation>Beskrivning</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="783"/>
        <source>No description available.</source>
        <translation>Ingen beskrivning finns tillgänglig.</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="804"/>
        <source>Similar Games</source>
        <translation>Liknande spel</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="463"/>
        <location filename="../LibraryListView.qml" line="538"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="24"/>
        <source>Search %L1 Games...</source>
        <translation>Sök %L1 spel...</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="108"/>
        <source>All (%L1)</source>
        <translation>Alla (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="151"/>
        <source>Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="152"/>
        <source>Adventure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="153"/>
        <source>Arcade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="154"/>
        <source>Board</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="155"/>
        <source>Blocks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="156"/>
        <source>Card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="157"/>
        <source>Kids</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="158"/>
        <source>Logic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="159"/>
        <source>RolePlaying</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="160"/>
        <source>Shooter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="161"/>
        <source>Simulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="162"/>
        <source>Sports</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="163"/>
        <source>Strategy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="180"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="189"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="466"/>
        <location filename="../LibraryListView.qml" line="541"/>
        <location filename="../LibraryListView.qml" line="556"/>
        <location filename="../LibraryListView.qml" line="610"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="495"/>
        <source>Servers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="577"/>
        <source>Resolve Error</source>
        <translation>Åtgärda fel</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="589"/>
        <source>Clear error</source>
        <translation>Rensa fel</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="594"/>
        <source>Mark as installed</source>
        <translation>Markera som installerad</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="601"/>
        <source>Mark as uninstalled</source>
        <translation>Markera som avinstallerad</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="832"/>
        <source>Reviews</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="837"/>
        <source>This game has no reviews.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1008"/>
        <source>Releases</source>
        <translation>Utgåvor</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1013"/>
        <source>No release information available.</source>
        <translation>Ingen releaseinformation tillgänglig.</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1036"/>
        <source>Version %1</source>
        <translation>Version %1</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1051"/>
        <source>No release description available.</source>
        <translation>Ingen releasebeskrivning finns tillgänglig.</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1068"/>
        <source>Anti-Features</source>
        <translation>Anti-funktioner</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1081"/>
        <source>This game requires NonFree assets.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1083"/>
        <source>This game requires NonFree network services.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1096"/>
        <source>Developer</source>
        <translation>Utvecklare</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1110"/>
        <source>License</source>
        <translation>Licens</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1139"/>
        <source>Links</source>
        <translation>Länkar</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1169"/>
        <source>Homepage</source>
        <translation>Hemsida</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1171"/>
        <source>Bug Tracker</source>
        <translation>Felsökare</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1173"/>
        <source>Help</source>
        <translation>Hjälp</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1175"/>
        <source>FAQ</source>
        <translation>Frågor och svar</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1178"/>
        <source>Donate</source>
        <translation>Donera</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1180"/>
        <source>Translation</source>
        <translation>Översättning</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1182"/>
        <source>Unknown</source>
        <translation>Okänd</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1184"/>
        <source>Manifest</source>
        <translation>Manifest</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1186"/>
        <source>Contact</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NavigationBar</name>
    <message>
        <source>Categories</source>
        <translation type="vanished">Kategorier</translation>
    </message>
    <message>
        <source>Installed successfully.</source>
        <translation type="vanished">Installerat framgångsrikt.</translation>
    </message>
    <message>
        <source>Uninstalled successfully.</source>
        <translation type="vanished">Avinstallerat framgångsrikt.</translation>
    </message>
    <message>
        <source>Updated successfully.</source>
        <translation type="vanished">Uppdateringen lyckades.</translation>
    </message>
    <message>
        <source>An error occurred.</source>
        <translation type="vanished">Ett fel inträffade.</translation>
    </message>
    <message>
        <source>‹</source>
        <translation type="vanished">‹</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="18"/>
        <source>Library</source>
        <translation>Bibliotek</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="29"/>
        <source>Servers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="40"/>
        <source>Settings</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="61"/>
        <source>Show games in a list view.</source>
        <translation>Visa spel i listvy.</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="71"/>
        <source>Show games in a grid view.</source>
        <translation>Visa spel i rutnätsvy.</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="80"/>
        <source>Reset settings to defaults.</source>
        <translation>Återställ inställningarna till standardvärdena.</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="92"/>
        <source>Check For Updates</source>
        <translation>Sök efter uppdateringar</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="96"/>
        <source>Update All</source>
        <translation>Uppdatera alla</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="115"/>
        <source>You have operations pending.</source>
        <translation>Du har väntande åtgärder.</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="121"/>
        <source>Close Anyway</source>
        <translation>Stäng ändå</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="127"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <source>⋮</source>
        <translation type="vanished">⋮</translation>
    </message>
    <message>
        <source>Reset All</source>
        <translation type="vanished">Återställ alla</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="100"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <source>Show Tray Icon</source>
        <translation type="vanished">Visa aktivitetsikon</translation>
    </message>
</context>
<context>
    <name>ServersView</name>
    <message>
        <location filename="../ServersView.qml" line="66"/>
        <location filename="../ServersView.qml" line="72"/>
        <location filename="../ServersView.qml" line="89"/>
        <location filename="../ServersView.qml" line="95"/>
        <source>?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ServersView.qml" line="174"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ServersView.qml" line="182"/>
        <source>Show empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ServersView.qml" line="190"/>
        <source>%L1 servers found.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsView</name>
    <message>
        <location filename="../SettingsView.qml" line="28"/>
        <source>Always Show Logs</source>
        <translation>Visa alltid loggar</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="35"/>
        <source>Notifications Enabled</source>
        <translation>Aviseringar aktiverade</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="46"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="69"/>
        <source>Reset Database</source>
        <translation>Återställ databas</translation>
    </message>
</context>
</TS>
