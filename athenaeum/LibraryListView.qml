import QtQuick 2.9
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.15
import Athenaeum 1.0


Item {
    id: libraryListView
    anchors.fill: parent
    // color: Material.background


    /* Search Bar */
    TextField {
        id: searchField
        leftPadding: 10
        rightPadding: 10
        anchors.top: parent.top
        anchors.bottom: buttonbox.top
        width: listView.width
        color: Material.foreground
        placeholderText: qsTr('Search %L1 Games...').arg(library.filter.length)
        onTextChanged: {
            library.searchValue = text
        }
        Keys.onEscapePressed: {
            text = ''
        }

        Button {
            id: clearText
            anchors { 
                top: parent.top
                right: parent.right
                bottom: parent.bottom 
            }

            background: Rectangle {
                anchors.fill: parent
                color: tr
            }

            visible: searchField.text
            icon.source: 'icons/close.svg'
            icon.height: 15
            icon.width: 15

            onClicked: {
                searchField.text = ""
                searchField.forceActiveFocus()
            }
        }
    }
    /* Game List */
    Flow {
        id:buttonbox
        anchors.top: searchField.bottom
        // spacing: 2
        ComboBox {
            id: filterCombo
            width: listView.width - filterButton.width
            
            currentIndex: getFilterIndex(library.filterValue)
            
            property string filterIndex: library.filterValue
            onFilterIndexChanged: {
                currentIndex = getFilterIndex(library.filterValue)
            }
            onModelChanged: {
                currentIndex = getFilterIndex(library.filterValue)
            }
            onActivated: {
                library.filterValue = getFilterKey(index)
                searchField.text = ''
            }
            function getFilterIndex(key) {
                switch(key) {
                    case 'all':
                        return 0;
                    case 'installed':
                        return 1;
                    case 'recent':
                        return 2;
                    case 'has_updates':
                        return 3;
                    case 'processing':
                        return 4;
                }
            }
            function getFilterKey(index) {
                switch(index) {
                    case 0:
                        return 'all';
                    case 1:
                        return 'installed';
                    case 2:
                        return 'recent';
                    case 3:
                        return 'has_updates';
                    case 4:
                        return 'processing';
                }
            }
        
            model: [
                qsTr('All (%L1)').arg(library.allCount),
                qsTr('Installed (%L1)').arg(library.installedCount),
                qsTr('Recent (%L1)').arg(library.recentCount),
                qsTr('Has Updates (%L1)').arg(library.hasUpdatesCount),
                qsTr('Processing (%L1)').arg(library.processingCount)
            ]
            validator: IntValidator {
                top: 5
                bottom: 0
            }
        }
        Button {
            flat: true
            id: filterButton
            icon.source: 'icons/filter.svg'
            width: 30
            highlighted: tagGrid.activeTags.length
            onClicked: {
                filterPopup.opened ? filterPopup.close() : filterPopup.open()
            }
            Popup {
                id: filterPopup
                visible: false
                x: parent.width
                closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

                Column {
                    anchors.fill: parent
                    Grid {
                        id: tagGrid
                        property var activeTags: []
                        columns: 3
                        spacing: 0

                        ButtonGroup {
                            id: tagGroup
                            buttons: tagRepeater.children
                            exclusive: false
                        }

                        Repeater {
                            id: tagRepeater
                            model: [
                                { id: 'Action', text: qsTr('Action') },
                                { id: 'Adventure', text: qsTr('Adventure') },
                                { id: 'Arcade', text: qsTr('Arcade') },
                                { id: 'Board', text: qsTr('Board') },
                                { id: 'Blocks', text: qsTr('Blocks') },
                                { id: 'Card', text: qsTr('Card') },
                                { id: 'Kids', text: qsTr('Kids') },
                                { id: 'Logic', text: qsTr('Logic') },
                                { id: 'RolePlaying', text: qsTr('RolePlaying') },
                                { id: 'Shooter', text: qsTr('Shooter') },
                                { id: 'Simulation', text: qsTr('Simulation') },
                                { id: 'Sports', text: qsTr('Sports') },
                                { id: 'Strategy', text: qsTr('Strategy') }
                            ]
                            delegate: CheckBox {
                                text: tagRepeater.model[index].text
                                ButtonGroup.group: tagGroup
                                onCheckedChanged: {
                                    tagGrid.activeTags = tagGrid.activeTags.filter(tag => tag !== tagRepeater.model[index].id)
                                    if (checked) {
                                        tagGrid.activeTags = tagGrid.activeTags.concat(tagRepeater.model[index].id)
                                    }
                                }    
                            }
                        }
                    }
                    RowLayout {
                        width: parent.width
                        Button {
                            text: qsTr('Apply')
                            Layout.fillWidth: true
                            onClicked: {
                                library.tagsValue = tagGrid.activeTags
                            }
                        }
                        Button {
                            enabled: tagGrid.activeTags.length
                            Layout.fillWidth: true
                            text: qsTr('Clear')
                            onClicked: {
                                tagGroup.checkState = Qt.Unchecked
                                tagGrid.activeTags = []
                                library.tagsValue = tagGrid.activeTags
                            }
                        }
                    }
                }
            }
        }
    }
    ListView {
        id: listView
        anchors.top: buttonbox.bottom
        anchors.bottom: parent.bottom
        model: library.filter
        width: 200
        ScrollBar.vertical: ScrollBar { }
        boundsBehavior: Flickable.StopAtBounds
        keyNavigationEnabled: true
        // focus: true
        clip: true

        onModelChanged: {
            currentIndex = library.getIndexForCurrentGame()
        }

        Keys.onUpPressed: {
            decrementCurrentIndex()
            window.indexUpdated(currentIndex)
        }
        Keys.onDownPressed: {
            incrementCurrentIndex()
            window.indexUpdated(currentIndex)
        }

        delegate: Component {
            id: delegateComponent
            Rectangle {
                // anchors.left: parent.left
                // anchors.right: parent.right
                height: 35
                width: 200
                id: rect
                border.color: ListView.isCurrentItem || itemMouseArea.containsMouse ? Material.accent : tr
                border.width: 1
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        listView.currentIndex = index
                        window.indexUpdated(index)
                        listView.forceActiveFocus()
                    }
                    id: itemMouseArea
                    hoverEnabled: true
                }
                // color: ListView.isCurrentItem ? Material.accent : itemMouseArea.containsMouse ? Material.accent : Material.background
                color: tr
                Rectangle {
                    id: gameIcon
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.margins: 1
                    width: parent.height
                    height: parent.height
                    color: tr
                    Image {
                        anchors.fill: parent
                        anchors.margins: 5
                        fillMode: Image.PreserveAspectFit
                        source: iconSmall
                    }
                }
                Text {
                    // color: parent.ListView.isCurrentItem ? Material.background : itemMouseArea.containsMouse ? Material.background : Material.foreground
                    color: Material.foreground
                    clip: true
                    width: parent.width
                    anchors.left: gameIcon.right
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    text: name
                    anchors.topMargin: 5
                    anchors.rightMargin: 5
                    anchors.bottomMargin: 5
                    verticalAlignment: Text.AlignVCenter
                }
                BusyIndicator {
                    visible: true
                    height: parent.height
                    width: parent.height
                    id: gameProcessing
                    anchors.right: parent.right
                    running: processing
                }
                Rectangle {
                    visible: false
                    height: parent.height
                    width: parent.height
                    anchors.right: parent.right
                    color: tr
                    Rectangle {
                        width: childrenRect.width
                        height: childrenRect.height
                        anchors.centerIn: parent
                        //color: sel
                        radius: 3
                        Text {
                            text: qsTr('New')
                            font.pixelSize: 12
                            padding: 3
                            //color: tc
                        }
                    }
                }
            }
        }
    }
    /* Game Detail Pane */
    Rectangle {
        id: gameDetails
        anchors.left: listView.right
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        color: Material.background
        Text {
            visible: !library.currentGame.id.length
            text: qsTr('Nothing seems to be here.')
            anchors.centerIn: parent
            color: Material.primary
            font.italic: true
            font.pixelSize: 14
        }
        Flickable {
            visible: library.currentGame.id.length
            anchors.fill: parent
            contentHeight: col.height
            contentWidth: parent.width
            ScrollBar.vertical: ScrollBar { }
            boundsBehavior: Flickable.StopAtBounds
            Column {
                id: col
                width: parent.width
                spacing: 40
                
                /* Header */
                Rectangle {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.rightMargin: 40
                    anchors.leftMargin: 40

                    color: Material.background
                    height: childrenRect.height + 40

                    Rectangle {
                        anchors.top: parent.top
                        anchors.left: parent.left
                        anchors.right: gameTitle.left
                        // anchors.bottom: parent.bottom
                        anchors.topMargin: 40

                        width: 128
                        height: 128
                        id: gameLogo

                        color: Material.primary
                        radius: 10
                        Image {
                            id: img
                            anchors.fill: parent
                            fillMode: Image.PreserveAspectFit
                            source: library.currentGame.iconLarge
                        }
                    }
                    Text {
                        id: gameTitle
                        anchors.top: parent.top
                        anchors.left: gameLogo.right
                        anchors.right: parent.right
                        anchors.topMargin: 40

                        leftPadding: 20

                        color: Material.foreground
                        text: library.currentGame.name


                        fontSizeMode: Text.VerticalFit
                        font.pixelSize: 48
                        minimumPixelSize: 30;

                        elide: Label.ElideRight

                        horizontalAlignment: Text.AlignLeft
                        wrapMode: Text.WordWrap
                    }
                    Text {
                        id: gameSummary
                        anchors.top: gameTitle.bottom
                        anchors.left: gameLogo.right
                        anchors.right: parent.right
                        leftPadding: 20

                        color: Material.foreground
                        text: library.currentGame.summary

                        fontSizeMode: Text.VerticalFit
                        font.pixelSize: 16
                        minimumPixelSize: 10;
                        elide: Label.ElideRight

                        horizontalAlignment: Text.AlignLeft
                        wrapMode: Text.WordWrap
                    }
                    Row {
                        anchors.top: gameSummary.bottom
                        anchors.left: gameLogo.right
                        anchors.right: parent.right
                        spacing: 10
                        leftPadding: 20
                        topPadding: 10
                        Button {
                            visible: !library.currentGame.installed
                            enabled: !library.currentGame.processing
                            // onClicked: {
                            //     window.installGame(library.currentGame.id)
                            // }
                            onClicked: {
                                installPopup.open()
                            }
                            icon.source: 'icons/download.svg'
                            text: qsTr('Install')
                            Popup {
                                id: installPopup
                                parent: stackView
                                x: Math.round((parent.width - width) / 2)
                                y: Math.round((parent.height - height) / 2)
                                modal: true
                                dim: true
                                focus: true
                                contentItem: Column {
                                    spacing: 20
                                    Text {
                                        visible: library.currentGame.downloadSize
                                        color: Material.foreground
                                        font.pixelSize: 20
                                        text:  qsTr('Download Size:\t%1')
                                            .arg(library.currentGame.downloadSize)
                                    }
                                    Text {
                                        visible: library.currentGame.installedSize
                                        color: Material.foreground
                                        font.pixelSize: 20
                                        text:  qsTr('Installed Size:\t%1')
                                            .arg(library.currentGame.installedSize)
                                    }
                                    Text {
                                        anchors.horizontalCenter: parent.horizontalCenter
                                        color: Material.foreground
                                        font.pixelSize: 20
                                        text: qsTr('Install Game?')
                                    }
                                    Row {
                                        spacing: 20
                                        anchors.horizontalCenter: parent.horizontalCenter
                                        Button {
                                            onClicked: {
                                                window.installGame(library.currentGame.id)
                                                installPopup.close()
                                            }
                                            text: qsTr('Yes')
                                        }
                                        Button {
                                            text: qsTr('Cancel')
                                            onClicked: {
                                                installPopup.close()
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        Button {
                            id: playButton
                            visible:  library.currentGame.installed
                            enabled: !library.currentGame.playing
                            onClicked: {
                                window.playGame(library.currentGame.id)
                            }
                            highlighted: true
                            icon.source: 'icons/play.svg'
                            text: library.currentGame.playing ? qsTr('In-Game') : qsTr('Play')

                        }
                        Button {
                            id: serversButton
                            visible:  playButton.visible && !!serverProvider.availableGames[library.currentGame.id]
                            onClicked: {
                                enter(serversView, library.currentGame.id)
                            }
                            highlighted: false
                            icon.source: 'icons/servers.svg'
                            text: qsTr('Servers')

                        }
                        Button {
                            visible: library.currentGame.hasUpdate && library.currentGame.installed
                            enabled: !library.currentGame.playing && !library.currentGame.processing
                            onClicked: {
                                window.updateGame(library.currentGame.id)
                            }
                            text: qsTr('Update')
                        }
                        Button {
                            text: qsTr('Uninstall')
                            icon.source: 'icons/trash.svg'
                            visible: library.currentGame.installed
                            enabled: !library.currentGame.processing
                            onClicked: {
                                uninstallPopup.open()
                            }
                            Popup {
                                id: uninstallPopup
                                parent: stackView
                                x: Math.round((parent.width - width) / 2)
                                y: Math.round((parent.height - height) / 2)
                                modal: true
                                dim: true
                                focus: true
                                contentItem: Column {
                                    spacing: 20
                                    Text {
                                        anchors.horizontalCenter: parent.horizontalCenter
                                        color: Material.foreground
                                        font.pixelSize: 20
                                        text: qsTr('Are you sure?')
                                    }
                                    Row {
                                        spacing: 20
                                        anchors.horizontalCenter: parent.horizontalCenter
                                        Button {
                                            onClicked: {
                                                window.uninstallGame(library.currentGame.id)
                                                uninstallPopup.close()
                                            }
                                            text: qsTr('Yes')
                                        }
                                        Button {
                                            text: qsTr('Cancel')
                                            onClicked: {
                                                uninstallPopup.close()
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        Button {
                            visible: library.currentGame.processing
                            onClicked: {
                                window.cancelGame(library.currentGame.id)
                            }
                            icon.source: 'icons/close.svg'
                            text: qsTr('Cancel')
                        }
                        Button {
                            visible: library.currentGame.error
                            onClicked: {
                                resolveErrorsPopup.open()
                            }
                            Popup {
                                id: resolveErrorsPopup
                                parent: stackView
                                x: Math.round((parent.width - width) / 2)
                                y: Math.round((parent.height - height) / 2)
                                modal: true
                                dim: true
                                focus: true
                                contentItem: Column {
                                    spacing: 20
                                    Text {
                                        anchors.horizontalCenter: parent.horizontalCenter
                                        color: Material.foreground
                                        font.pixelSize: 20
                                        text: qsTr('Resolve Error')
                                    }
                                    Column {
                                        spacing: 10
                                        anchors.horizontalCenter: parent.horizontalCenter
                                        Button {
                                            anchors.left: parent.left
                                            anchors.right: parent.right
                                            onClicked: {
                                                window.clearErrors(library.currentGame.id)
                                                resolveErrorsPopup.close()
                                            }
                                            text: qsTr('Clear error')
                                        }
                                        Button {
                                            anchors.left: parent.left
                                            anchors.right: parent.right
                                            text: qsTr('Mark as installed')
                                            onClicked: {
                                                window.markInstalled(library.currentGame.id)
                                                resolveErrorsPopup.close()
                                            }
                                        }
                                        Button {
                                            text: qsTr('Mark as uninstalled')
                                            onClicked: {
                                                window.markUninstalled(library.currentGame.id)
                                                resolveErrorsPopup.close()
                                            }
                                        }
                                        Button {
                                            anchors.left: parent.left
                                            anchors.right: parent.right
                                            text: qsTr('Cancel')
                                            onClicked: {
                                                resolveErrorsPopup.close()
                                            }
                                        }
                                    }
                                }
                            }
                            highlighted: true
                            Material.accent: Material.Red
                            icon.source: 'icons/exclamation.svg'
                        }
                    }
                }

                /* Screenshots */
                Column {
                    width: parent.width
                    visible: library.currentGame.screenshots.length
                    Rectangle {
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.rightMargin: 40
                        anchors.leftMargin: 40
                        clip: true
                        height: 350
                        color: "black"
                        
                        Image {
                            anchors.left: screenshotsList.left
                            anchors.right: parent.right
                            anchors.bottom: parent.bottom
                            anchors.top: parent.top
                            fillMode: Image.PreserveAspectCrop
                            source:  visible ? (library.currentGame.screenshots[screenshotsList.currentIndex] ? library.currentGame.screenshots[screenshotsList.currentIndex].thumbUrl : '') : ''
                            opacity: 0.6
                        }
                        
                        Rectangle {
                            id: screenshotsListBackground
                            width: 100
                            anchors.top: parent.top
                            anchors.bottom: parent.bottom
                            anchors.left: parent.left
                            color: "black"
                            opacity: 0.5
                        }
                        
                        ListView {
                                id: screenshotsList
                                width: 100
                                anchors.top: parent.top
                                anchors.bottom: parent.bottom
                                anchors.left: parent.left
                                clip: true
                                model: library.currentGame.screenshots
                                spacing: 5
                                boundsBehavior: Flickable.StopAtBounds
                                ScrollBar.vertical: ScrollBar { }
                                delegate: Rectangle {
                                    height: 60
                                    width: parent.width
                                    color: Material.primary
                                    Image {
                                        anchors.fill: parent
                                        anchors.margins: 1
                                        fillMode: Image.PreserveAspectFit
                                        source: thumbUrl
                                        opacity: 1.0
                                    }
                                    MouseArea {
                                        anchors.fill: parent
                                        onClicked: {
                                            screenshotsList.currentIndex = index
                                        }
                                        hoverEnabled: true
                                        id: thumbMouseArea
                                    }
                                    border.color: ListView.isCurrentItem ? Material.accent : thumbMouseArea.containsMouse ? Material.foreground : Material.primary
                                }
                            }
                        
                        BusyIndicator {
                            id: previewLoadingIndicator
                            anchors.centerIn: parent
                            running: largeView.progress != 1.0
                        }

                        Image {
                            id: largeView
                            anchors.left: screenshotsList.left
                            anchors.right: parent.right
                            anchors.bottom: parent.bottom
                            anchors.top: parent.top
                            fillMode: Image.PreserveAspectFit
                            source: visible ? (library.currentGame.screenshots[screenshotsList.currentIndex] ? library.currentGame.screenshots[screenshotsList.currentIndex].sourceUrl : '') : ''
                            MouseArea {
                                anchors.centerIn: parent
                                width: parent.paintedWidth
                                height: parent.paintedHeight
                                onClicked: {
                                    fullscreenPreview.open()
                                }
                            }
                        }
                        FullscreenPreview {
                            id: fullscreenPreview
                            source: largeView.source
                        }
                    }
                }

                /* Logs */
                Rectangle {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.rightMargin: 40
                    anchors.leftMargin: 40
                    color: "black"
                    height: 160
                    visible: library.currentGame.error || library.currentGame.processing || (settings.alwaysShowLogs && library.currentGame.installed)

                    Flickable {
                        id: testFlick
                        anchors.fill: parent

                        // ScrollBar.vertical: ScrollBar {
                        //     policy: ScrollBar.AlwaysOn }
                        clip: true
                        boundsBehavior: Flickable.StopAtBounds

                        TextArea {
                            id: ta
                            onContentHeightChanged: {
                                testFlick.contentY = (contentHeight <= 150 ? 0 : contentHeight - 150)
                            }
                            color: "white"
                            readOnly: true
                            text: library.currentGame.log
                            background: Rectangle {
                                anchors.fill: parent
                                color: "black"
                            }
                        }
                    }
                }

                /* Body */
                Grid {
                    id: bodyGrid
                    anchors.left: parent.left
                    anchors.right: parent.right
                    columns: 2
                    spacing: 40
                    leftPadding: 40
                    rightPadding: 40
                    topPadding: 10

                    Column {
                        width: parent.width - miscInfo.width - bodyGrid.spacing - bodyGrid.leftPadding - bodyGrid.rightPadding
                        spacing: 10

                        /* Description */
                        Label {
                            visible: library.currentGame.description
                            id: descHeading
                            width: parent.width
                            font.pixelSize: 24
                            text: qsTr('Description')
                            wrapMode: Text.WrapAnywhere
                        }
                        Label {
                            visible: !library.currentGame.description
                            text: qsTr('No description available.')
                            font.italic: true
                        }
                        Label {
                            visible: library.currentGame.description
                            topPadding: 10
                            bottomPadding: 10
                            width: parent.width
                            textFormat: Text.RichText
                            font.pixelSize: 16
                            text: library.currentGame.description
                            wrapMode: Text.WordWrap
                        }


                        /* Similar */
                        Label {
                            id: similarHeading
                            visible: similarGrid.model ? similarGrid.model.length : false
                            width: parent.width
                            font.pixelSize: 24
                            text: qsTr('Similar Games')
                            wrapMode: Text.WrapAnywhere
                        }
                        Flow {
                            id: similarFlow
                            width: parent.width
                            spacing: 10
                            Repeater {
                                id: similarGrid
                                model: library.similarGames
                                delegate: ToolButton {
                                    icon.source: similarGrid.model[index].iconSmall
                                    icon.color: '#00000000'
                                    text: similarGrid.model[index].name
                                    font.capitalization: Font.MixedCase
                                    font.pixelSize: 20
                                    onClicked: {
                                        library.currentGame = similarGrid.model[index]
                                    }
                                }
                            }
                        }

                        /* Reviews */
                        Label {
                            id: reviewsHeading
                            width: parent.width
                            font.pixelSize: 24
                            text: qsTr('Reviews')
                            wrapMode: Text.WrapAnywhere
                        }
                        Label {
                            visible: !reviewsGrid.model.length && !reviewsFlow.loadingReviews
                            text: qsTr('This game has no reviews.')
                            font.italic: true
                        }
                        BusyIndicator {
                            visible: reviewsFlow.loadingReviews
                            id: reviewsLoadingIndicator
                            running: reviewsFlow.loadingReviews
                        }
                        Flow {
                            id: reviewsFlow
                            width: parent.width
                            spacing: 10

                            property bool loadingReviews: false

                            Component.onCompleted: {
                                library.currentGameChanged.connect(retrieveReviews)
                            }

                            function retrieveReviews() {
                                reviewsGrid.model = []
                                loadingReviews = true
                                
                                if (!library.currentGame.id) {
                                    return
                                }

                                makeRequest({
                                    method: 'POST',
                                    url: 'https://odrs.gnome.org/1.0/reviews/api/fetch',
                                    params: reviewsProvider.getFetchParams(library.currentGame.id)
                                })
                                .then(function (response) {
                                    let reviews = JSON.parse(response.response)
                                    reviews = reviews.filter(review => "rating" in review)
                                    if (reviews.length) {
                                        if (library.currentGame.id !== response.params.app_id) {
                                            throw 'stale'
                                        }

                                    }
                                    reviewsGrid.model = reviews
                                    loadingReviews = false
                                })
                                .catch(function (err) {
                                    if (typeof err === 'string' && err == 'stale') {

                                    } else {
                                        print('Error loading reviews!', err.statusText)
                                        reviewsGrid.model = []
                                        loadingReviews = false  
                                    }
                                })
                            }

                            property int minWidth: 250
                            property int count: width / minWidth
                            property int cellWidth: width > minWidth ? (width / count) - spacing : width

                            Repeater {
                                id: reviewsGrid
                                model: []
                                function formatTimestamp(ts) {
                                    var t = new Date( 0 );
                                    t.setSeconds(ts);
                                    return t.toLocaleDateString();
                                }
                                delegate: Frame {
                                    Material.elevation: 5
                                    width: reviewsFlow.cellWidth
                                    height: 150

                                    Popup {
                                        id: reviewPopup
                                        anchors.centerIn: Overlay.overlay
                                        width: 300
                                        dim: true
                                        modal: true
                                        focus: true
                                        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
                                        Column {
                                            anchors.fill: parent
                                            Label {
                                                width: parent.width
                                                font.pixelSize: 16
                                                font.italic: !reviewsGrid.model[index].description
                                                text: reviewsGrid.model[index].description
                                                wrapMode: Text.WordWrap
                                                bottomPadding: 10
                                                elide: Text.ElideRight
                                            }
                                            Label {
                                                width: parent.width
                                                font.pixelSize: 12
                                                text: reviewsGrid.model[index].user_display
                                                wrapMode: Text.WordWrap
                                            }
                                            Label {
                                                width: parent.width
                                                font.pixelSize: 10
                                                text: reviewsGrid.formatTimestamp(reviewsGrid.model[index].date_created)
                                                wrapMode: Text.WordWrap
                                            }
                                        }
                                        MouseArea {
                                            anchors.fill: parent
                                            parent: Overlay.overlay
                                            onClicked: {
                                                close()
                                            }
                                        }
                                    }
                                    MouseArea {
                                        anchors.fill: parent
                                        cursorShape: Qt.PointingHandCursor
                                        onClicked: {
                                            reviewPopup.open()
                                        }
                                    }
                                    Image {
                                        id: thumbImage
                                        anchors.bottom: parent.bottom
                                        anchors.right: parent.right
                                        
                                        fillMode: Image.PreserveAspectFit
                                        source: reviewsGrid.model[index].rating >= "60" ? 'icons/thumbs-up.svg' : 'icons/thumbs-down.svg'
                                        sourceSize.width: 40
                                        sourceSize.height: 40
                                    }
                                    ColorOverlay {
                                        anchors.fill: thumbImage
                                        source: thumbImage
                                        color: reviewsGrid.model[index].rating >= "60" ? "lightblue" : "coral"
                                    }
                                    Item {
                                        anchors.fill: parent
                                        Label {
                                            width: parent.width
                                            height: parent.height - reviewName.height - reviewDate.height
                                            font.pixelSize: 16
                                            font.italic: !reviewsGrid.model[index].description
                                            text: reviewsGrid.model[index].description
                                            wrapMode: Text.WordWrap
                                            bottomPadding: 10
                                            elide: Text.ElideRight
                                        }
                                        Label {
                                            id: reviewName
                                            width: parent.width
                                            font.pixelSize: 12
                                            text: reviewsGrid.model[index].user_display
                                            wrapMode: Text.WordWrap
                                            anchors.bottom: reviewDate.top
                                        }
                                        Label {
                                            id: reviewDate
                                            width: parent.width
                                            font.pixelSize: 10
                                            text: reviewsGrid.formatTimestamp(reviewsGrid.model[index].date_created)
                                            wrapMode: Text.WordWrap
                                            anchors.bottom: parent.bottom
                                        }
                                    }
                                }
                            }
                        }

                        /* Releases */
                        Label {
                            id: releaseHeading
                            font.pixelSize: 24
                            text: qsTr('Releases')
                            wrapMode: Text.WrapAnywhere
                        }
                        Label {
                            visible: !library.currentGame.releases.length
                            text: qsTr('No release information available.')
                            font.italic: true
                        }
                        ListView {
                            visible: library.currentGame.releases.length
                            model: library.currentGame.releases
                            width: parent.width
                            height: contentHeight
                            spacing: 10
                            // enabled: false
                            boundsBehavior: Flickable.StopAtBounds
                            delegate: Column {
                                width: parent.width
                                function formatTimestamp(ts) {
                                    var t = new Date( 0 );
                                    t.setSeconds(ts);
                                    return t.toLocaleDateString();
                                }
                                Flow {
                                    width: parent.width
                                    spacing: 10
                                    Label {
                                        font.pixelSize: 20
                                        text: qsTr('Version %1').arg(version)
                                        wrapMode: Text.WrapAnywhere
                                    }
                                    Label {
                                        font.pixelSize: 12
                                        text: formatTimestamp(timestamp)
                                        wrapMode: Text.WrapAnywhere
                                    }
                                }
                                Label {
                                    topPadding: 10
                                    bottomPadding: 10
                                    width: parent.width
                                    font.pixelSize: 16
                                    font.italic: description ? false : true
                                    text: description || qsTr('No release description available.')
                                    wrapMode: Text.WrapAnywhere
                                }
                            }
                        }
                    }

                    /* Links and Categories */
                    Column {
                        id: miscInfo
                        width: 150
                        spacing: 10

                        Label {
                            visible: library.currentGame.antiFeatures.length
                            color: Material.color(Material.Red)
                            font.pixelSize: 20
                            text: qsTr('Anti-Features')
                            wrapMode: Text.WrapAnywhere
                        }
                        ListView {
                            visible: library.currentGame.antiFeatures.length
                            model: library.currentGame.antiFeatures
                            height: contentHeight
                            width: parent.width
                            spacing: 10
                            delegate: Label {
                                function getTitle(type) {
                                    switch(type) {
                                        case 'assets':
                                            return qsTr('This game requires NonFree assets.');
                                        case 'network':
                                            return qsTr('This game requires NonFree network services.');
                                    }
                                }
                                width: parent.width
                                color: Material.color(Material.Red)
                                font.pixelSize: 16
                                wrapMode: Text.WordWrap
                                text: getTitle(library.currentGame.antiFeatures[index])
                            }
                        }                        
                        Label {
                            visible: library.currentGame.developerName
                            font.pixelSize: 20
                            text: qsTr('Developer')
                            wrapMode: Text.WrapAnywhere
                        }
                        Label {
                            width: parent.width
                            visible: library.currentGame.developerName
                            font.pixelSize: 16
                            text: library.currentGame.developerName
                            wrapMode: Text.WrapAnywhere
                        }
                        
                        Label {
                            visible: library.currentGame.license
                            font.pixelSize: 20
                            text: qsTr('License')
                            wrapMode: Text.WrapAnywhere
                        }
                        Label {
                            width: parent.width
                            visible: library.currentGame.license
                            font.pixelSize: 16
                            text: library.currentGame.license
                            wrapMode: Text.WrapAnywhere
                            // Rectangle {
                            //     anchors.fill: parent
                            //     color: "red"
                            // }
                        }
//                         Text {
//                             color: Material.foreground
//                             font.pixelSize: 20
//                             text: qsTr('Hours Played')
//                             wrapMode: Text.WrapAnywhere
//                         }
//                         Text {
//                             color: Material.foreground
//                             font.pixelSize: 16
//                             text: qsTr('14 Hours')
//                             wrapMode: Text.WrapAnywhere
//                         }     
                        Label {
                            visible: library.currentGame.urls.length
                            font.pixelSize: 20
                            text: qsTr('Links')
                            wrapMode: Text.WrapAnywhere
                        }
                        ListView {
                            visible: library.currentGame.urls.length
                            model: library.currentGame.urls
                            id: linksList
                            height: contentHeight
                            width: contentWidth
                            delegate: Button {
                                MouseArea {
                                    anchors.fill: parent
                                    hoverEnabled: true
                                    cursorShape: Qt.PointingHandCursor
                                    onClicked: {
                                        Qt.openUrlExternally(url)
                                    }
                                }
                                icon.source: 'icons/' + urlIcon
                                topPadding: 0
                                leftPadding: 0
                                background: Rectangle {
                                    anchors.fill: parent
                                    color: tr
                                }

                                font.capitalization: Font.MixedCase
                                function getTitle(type) {
                                    switch(type) {
                                        case 'homepage':
                                            return qsTr('Homepage');
                                        case 'bugtracker':
                                            return qsTr('Bug Tracker');
                                        case 'help':
                                            return qsTr('Help');
                                        case 'faq':
                                            return qsTr('FAQ');
                                        case 'donation':
                                            icon.color = '#00000000';
                                            return qsTr('Donate');
                                        case 'translate':
                                            return qsTr('Translation');
                                        case 'unknown':
                                            return qsTr('Unknown');
                                        case 'manifest':
                                            return qsTr('Manifest');
                                        case 'contact':
                                            return qsTr('Contact');
                                    }
                                }
                                text: getTitle(type)
                            }
                        }
                    }
                }
            }
        }
    }
}